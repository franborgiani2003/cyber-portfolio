
//Navbar Effect

const navbar = document.querySelector('.header-navbar');

window.addEventListener('scroll', ()=>{
    if(window.scrollY > 40){
        navbar.classList.add('sticky');
    }else{
        navbar.classList.remove('sticky');
    }
});
//Menu Responsive

const menuBtn = document.querySelector('.hamburguer')
const menu = document.querySelector('.header-navbar-menu');

menuBtn.addEventListener('click', ()=>{
    menu.classList.toggle('active');
    menuBtn.classList.toggle('active');
    navbar.classList.add('sticky');
    menuBtn.addEventListener('click', ()=>{
        if(window.scrollY < 40){
            navbar.classList.toggle('sticky')
        }
    });
});
menu.addEventListener('click', ()=>{
    menu.classList.remove('active');
    menuBtn.classList.remove('active');
    navbar.classList.remove('active');
    window.addEventListener('scroll',()=>{
        if(window.scrollY < 40){
            navbar.classList.remove('active');
        }
    });
});